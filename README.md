<div align="center">

<img src="assets/logo.jpg" height="100"  />

[![Latest Release](https://gitlab.com/ASTRELION/astrelplugin/-/badges/release.svg)](https://gitlab.com/ASTRELION/astrelplugin/-/releases)
[![pipeline status](https://gitlab.com/ASTRELION/astrelplugin/badges/main/pipeline.svg)](https://gitlab.com/ASTRELION/astrelplugin/-/commits/main)
[![License](https://img.shields.io/gitlab/license/37665917)](LICENSE)
</div>

# AstrelPlugin

A Minecraft plugin package to use as a base for all of my plugins.

[**A bare-bones example implementation can be found here.**](src/test)

**Plugins using this package:**

- [LaunchMe](https://gitlab.com/ASTRELION/launchme)
  [![bStats Servers](https://img.shields.io/bstats/servers/9918?color=00695C)](https://bstats.org/plugin/bukkit/LaunchMe/9918)
  [![bStats Servers](https://img.shields.io/bstats/players/9918?color=00695C)](https://bstats.org/plugin/bukkit/LaunchMe/9918)
- [Better Leads](https://gitlab.com/ASTRELION/betterleads)
  [![bStats Servers](https://img.shields.io/bstats/servers/15788?color=00695C)](https://bstats.org/plugin/bukkit/BetterLeads/15788)
  [![bStats Servers](https://img.shields.io/bstats/players/15788?color=00695C)](https://bstats.org/plugin/bukkit/BetterLeads/15788)

## Features

| Feature                                                                           | Description                                                                          |
|-----------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|
| AstrelCommand | Command handler using ACF implementing basic commands like `reload`, `version`, etc. |
| AstrelUtil                                                                        | Static utility functions like `clamp` and tick to second conversions                 |
| AstrelConfiguration                                                               | AstrelResourceManager for config.yml files                                           |
| AstrelLanguage                                                                    | AstrelResourceManager for lang.yml files                                             |
| AstrelPermissions                                                                 | Basic permission handler                                                             |
| AstrelResourceManager                                                             | Manages YAML plugin resources like configs                                           |
| AstrelUpdateManager                                                               | Manages prompting when new updates are available                                     |

## Install

[**Package Registry**](https://gitlab.com/ASTRELION/astrelplugin/-/packages)  
*Versions ending like `-abcd1234` are subject to deletion*

*Replace `1.0.0` version in the following snippets with the most recent version [available here](https://gitlab.com/ASTRELION/astrelplugin/-/packages).*

<details>
<summary>Gradle (Groovy DSL)</summary>

```groovy
repositories
{
    maven { url "https://gitlab.com/api/v4/projects/37665917/packages/maven" }
}
```

```groovy
dependencies
{
    implementation "com.astrelion:AstrelPlugin:1.0.0"
}
```
</details>

<details>
<summary>Gradle (Kotlin DSL)</summary>

```kotlin
repositories {
    maven { url = "https://gitlab.com/api/v4/projects/37665917/packages/maven" }
}
```

```kotlin
dependencies {
    implementation("com.astrelion:AstrelPlugin:1.0.0")
}
```
</details>

<details>
<summary>Maven</summary>

```xml
<dependency>
    <groupId>com.astrelion</groupId>
    <artifactId>AstrelPlugin</artifactId>
    <version>1.0.0-33869b4a</version>
</dependency>
```

```shell
mvn dependency:get -Dartifact=com.astrelion:AstrelPlugin:1.0.0
```

```xml
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/37665917/packages/maven</url>
    </repository>
</repositories>

<distributionManagement>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/37665917/packages/maven</url>
    </repository>

    <snapshotRepository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/37665917/packages/maven</url>
    </snapshotRepository>
</distributionManagement>
```
</details>

## Features

### Commands

The command manager is initialized in `AstrelPlugin#onEnableBefore()`. 

Register the built-in command set with

`AstrelPlugin#onEnable()`  
```java
commandManager.getCommandReplacements().addReplacement("base", "example");
commandManager.getCommandReplacements().addReplacement("basealias", "ex");
commandManager.registerCommand(new AstrelCommand(this));
```

This will automatically create `example.help`, `example.version`, and `example.reload` permissions. The command then can be used with `/example` or `/ex`.

| Command         | Permission     | Description                                     |
|-----------------|----------------|-------------------------------------------------|
| `/base help`    | `base.help`    | Show the help dialog                            |
| `/base reload`  | `base.reload`  | Reloads the `config.yml` and `lang.yml` files   |
| `/base version` | `base.version` | Shows the plugin version and checks for updates |

`base` will be replaced with whatever was specified as a command replacement.

### Permissions

| Permission      | Description                                                                                |
|-----------------|--------------------------------------------------------------------------------------------|
| `base.*`        | Grants all permissions. **NOT automatically created, specify in `plugin.yml`**             |
| `base.help`     | Allows `/base help`. Created automatically.                                                |
| `base.reload`   | Allows `/base reload`. Created automatically.                                              |
| `base.update`   | User will get update notifications. **NOT automatically created, specify in `plugin.yml`** |
| `base.version`  | Allows `/base version`. Created automatically.                                             |

## Testing (for plugins that extend this)

### Useful Gradle Tasks

**Automatic Copy Jar Task**

This `copyToTestServer` task will automatically copy the built `.jar` file into your test server's plugin directory.

```groovy
tasks.register("copyToTestServer")
{
    // Remember to replace / with \\ on Windows!
    def testServerDir = "/path/to/your/test/server"

    copy
    {
        from layout.buildDirectory.dir("libs")
        include "*-${version}.jar"
        into testServerDir + "plugins"
    }
}

// if you don't use shadowJar, replace shadowJar with jar
copyToTestServer.dependsOn shadowJar
```

### Debug on a Test Server

**Example Start Script**

`start.sh`  
```shell
#!/bin/bash

java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -jar paper.jar --nogui
```

**Attach IntelliJ debugger**

1. Edit Configurations
2. Add new 'Remote JVM Debug'
3. Run debug with the new configuration
4. Build project for changes to take effect
