package com.astrelion.astrelplugin;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class AstrelResource
{
    protected final JavaPlugin plugin;
    protected final String resourceName;
    protected YamlConfiguration resource;

    public AstrelResource(JavaPlugin plugin, String resourceName)
    {
        this.plugin = plugin;
        this.resourceName = resourceName;

        updateResource();
    }

    /**
     * If the existing resource is outdated, save a new version with old values.
     * If the resource doesn't exist, saves the default configuration.
     */
    public void updateResource()
    {
        File file = new File(plugin.getDataFolder(), resourceName);

        // Replace existing file
        if (file.exists())
        {
            // Store old config
            YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(file);
            Map<String, Object> values = yamlConfiguration.getValues(true);

            // Save a default config
            plugin.saveResource(resourceName, true);
            yamlConfiguration = YamlConfiguration.loadConfiguration(file);

            // Load default config with old values, if the keys still exist
            for (String key : yamlConfiguration.getValues(true).keySet())
            {
                if (values.containsKey(key))
                {
                    yamlConfiguration.set(key, values.get(key));
                }
            }

            // Commit
            try
            {
                yamlConfiguration.save(file);
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
            this.resource = yamlConfiguration;
        }
        // Save default file
        else
        {
            if (plugin.getResource(resourceName) != null)
            {
                plugin.saveResource(resourceName, false);
            }
            else
            {
                try
                {
                    file.createNewFile();
                }
                catch (IOException e)
                {
                    plugin.getLogger().warning(String.format("Unable to create resource '%s'.", resourceName));
                }
            }
            this.resource = YamlConfiguration.loadConfiguration(file);
        }
    }

    /**
     * Reload the configuration from file.
     */
    public void reload()
    {
        File file = new File(plugin.getDataFolder(), resourceName);
        if (!file.exists()) updateResource();
        this.resource = YamlConfiguration.loadConfiguration(file);
    }

    public void save()
    {
        try
        {
            File file = new File(plugin.getDataFolder(), resourceName);
            resource.save(file);
        }
        catch (IOException e)
        {
            plugin.getLogger().warning(String.format("Unable to save resource '%s'.", resourceName));
        }
    }

    public YamlConfiguration getConfiguration()
    {
        return this.resource;
    }
}
