package com.astrelion.astrelplugin;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.*;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Base command for most plugins.
 * Implements help, version, and reload.
 * Replace %base with plugin base command.
 * Replace %basealias with an alias of the base.
 */
@CommandAlias("%base|%basealias")
@Description("Plugin base command")
public class AstrelCommand extends BaseCommand
{
    private final JavaPlugin plugin;
    private final AstrelConfiguration configuration;
    private final AstrelLanguage language;
    private final AstrelUpdate updateManager;
    private final String sourceURL;

    public AstrelCommand(
        JavaPlugin plugin,
        AstrelConfiguration configuration,
        AstrelLanguage language,
        AstrelUpdate updateManager,
        String sourceURL)
    {
        this.plugin = plugin;
        this.configuration = configuration;
        this.language = language;
        this.updateManager = updateManager;
        this.sourceURL = sourceURL;
    }

    @Default
    @HelpCommand
    @Description("Show help")
    @CommandPermission("%base.help")
    public void doHelp(CommandSender sender, CommandHelp help)
    {
        sender.sendMessage(plugin.getName());
        help.showHelp();
    }

    @Subcommand("reload|r")
    @Description("Reload config.yml and lang.yml from file")
    @CommandPermission("%base.reload")
    public void doReload(CommandSender sender)
    {
        this.configuration.reload();
        this.language.reload();
        sender.sendMessage(this.language.getMessage(AstrelLanguage.RELOAD));
    }

    @Subcommand("version|v")
    @Description("Display plugin version and check for updates")
    @CommandPermission("%base.version")
    public void doVersion(CommandSender sender)
    {
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () ->
        {
            sender.sendMessage(this.language.getMessage(
                AstrelLanguage.VERSION,
                this.updateManager.checkForUpdate(),
                plugin.getName(),
                plugin.getDescription().getVersion(),
                this.updateManager.getSourceVersionString(),
                this.sourceURL
            ));
        });
    }
}
