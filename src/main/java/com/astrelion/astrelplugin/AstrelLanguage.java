package com.astrelion.astrelplugin;

import org.bukkit.plugin.java.JavaPlugin;

import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AstrelLanguage extends AstrelResource
{
    public static final String LOG_CURRENT = "logCurrent";
    public static final String LOG_DISABLE = "logDisable";
    public static final String LOG_DISABLE_CONFIG = "logDisableConfig";
    public static final String LOG_ENABLE = "logEnable";
    public static final String LOG_NEW = "logNew";
    public static final String LOG_OLD = "logOld";
    public static final String LOG_UPDATE_FAIL = "logUpdateFail";
    public static final String NO_PERMISSION = "noPermission";
    public static final String RELOAD = "reload";
    public static final String UPDATE_PROMPT = "updatePrompt";
    public static final String VERSION = "version";

    public AstrelLanguage(JavaPlugin plugin)
    {
        super(plugin, "lang.yml");

        for (String s : new String[]{
                LOG_CURRENT,
                LOG_DISABLE,
                LOG_DISABLE_CONFIG,
                LOG_ENABLE,
                LOG_NEW,
                LOG_OLD,
                LOG_UPDATE_FAIL,
                NO_PERMISSION,
                RELOAD,
                UPDATE_PROMPT,
                VERSION})
        {
            if (!resource.contains(s))
            {
                throw new IllegalStateException(String.format("lang.yml must contain '%s' node.", s));
            }
        }
    }

    public String getMessage(String key, boolean which)
    {
        String message = getMessage(key);

        // match {|}
        Pattern pattern = Pattern.compile("\\{.*?\\|.*?\\}");
        Matcher matcher = pattern.matcher(message);

        // find each {|} and replace it with one of either side of |
        while (matcher.find())
        {
            String group = matcher.group()
                .replaceFirst("^\\{", "")
                .replaceFirst("\\}$", "");
            String[] split = group.split("\\|");
            message = message.replaceFirst("\\{.*?\\|.*?\\}", split[which ? 0 : 1]);

            matcher = pattern.matcher(message);
        }

        return message;
    }

    public String getMessage(String key)
    {
        return resource.getString(key);
    }

    public String getMessage(String key, String... args)
    {
        return MessageFormat.format(getMessage(key), (Object[]) args);
    }

    public String getMessage(String key, boolean which, String... args)
    {
        return MessageFormat.format(getMessage(key, which), (Object[]) args);
    }
}
