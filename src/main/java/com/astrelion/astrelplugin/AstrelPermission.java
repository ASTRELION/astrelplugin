package com.astrelion.astrelplugin;

import org.bukkit.plugin.java.JavaPlugin;

public class AstrelPermission
{
    public static String ALL;
    public static String HELP;
    public static String RELOAD;
    public static String UPDATE;
    public static String VERSION;

    protected JavaPlugin plugin;

    public AstrelPermission(JavaPlugin plugin)
    {
        this.plugin = plugin;
        String basename = AstrelUtil.baseName(plugin);
        ALL = basename + ".*";
        HELP = basename + ".help";
        RELOAD = basename + ".reload";
        UPDATE = basename + ".update";
        VERSION = basename + ".version";
    }
}
