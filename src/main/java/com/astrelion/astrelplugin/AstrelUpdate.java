package com.astrelion.astrelplugin;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.net.URL;
import java.net.URLConnection;
import java.util.*;

public class AstrelUpdate implements Listener
{
    protected final JavaPlugin plugin;
    protected final AstrelLanguage language;
    protected final String sourceURL;
    protected String sourceVersionString;
    protected int sourceVersion;
    protected boolean updateAvailable;
    protected Set<UUID> playersNotified;

    public AstrelUpdate(JavaPlugin plugin, AstrelLanguage language, String sourceURL)
    {
        this.plugin = plugin;
        this.language = language;
        this.sourceURL = sourceURL;

        playersNotified = new HashSet<>();
        sourceVersionString = "0.0.0";
        sourceVersion = 0;
        updateAvailable = false;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * Handles update alerts, if enabled.
     * Alerts every player with permission when they join the server.
     * @param event the PlayerJoinEvent
     */
    @EventHandler
    protected void onPlayerJoin(PlayerJoinEvent event)
    {
        if (AstrelConfiguration.PROMPT_UPDATES && updateAvailable)
        {
            Player player = event.getPlayer();

            if (player.hasPermission(AstrelPermission.UPDATE) && !playersNotified.contains(player.getUniqueId()))
            {
                promptPlayer(player);
            }
        }
    }

    /**
     * Checks for new updates and sends alerts to all online players
     * with update permission.
     */
    public void sendUpdateAlert()
    {
        // Check for new update
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () ->
        {
            if (AstrelConfiguration.PROMPT_UPDATES && checkForUpdate())
            {
                for (Player player : plugin.getServer().getOnlinePlayers())
                {
                    if (player.hasPermission(AstrelPermission.UPDATE) && !playersNotified.contains(player.getUniqueId()))
                    {
                        promptPlayer(player);
                    }
                }
            }
        });
    }

    public void promptPlayer(Player player)
    {
        player.sendMessage(language.getMessage(AstrelLanguage.UPDATE_PROMPT, this.sourceURL));
        playersNotified.add(player.getUniqueId());
    }

    /**
     * Calls checkForUpdate() inside an asynchronous task.
     */
    public void checkForUpdateAsync()
    {
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, this::checkForUpdate);
    }

    /**
     * Check this version against the most recent source version.
     * @return true if a new update is available, false otherwise.
     */
    public boolean checkForUpdate()
    {
        updateAvailable = false;

        try
        {
            // Query URL
            URL url = new URL(this.sourceURL);
            URLConnection connection = url.openConnection();
            connection.getInputStream();
            URL resolvedURL = connection.getURL();

            // Resolve version integer
            String[] path = resolvedURL.toString().split("/");
            sourceVersionString = path[path.length - 1];
            sourceVersion = Integer.parseInt(sourceVersionString.replaceAll("\\D", ""));

            // Get this version
            String thisVersionString = plugin.getDescription().getVersion();
            int thisVersion = Integer.parseInt(thisVersionString.replaceAll("\\D", ""));

            // Alert
            if (sourceVersion > thisVersion)
            {
                plugin.getLogger().warning(this.language.getMessage(AstrelLanguage.LOG_OLD, thisVersionString, sourceVersionString, this.sourceURL));
                updateAvailable = true;
                return true;
            }
            else if (sourceVersion < thisVersion)
            {
                plugin.getLogger().info(this.language.getMessage(AstrelLanguage.LOG_NEW, thisVersionString, sourceVersionString));
            }
            else
            {
                plugin.getLogger().info(this.language.getMessage(AstrelLanguage.LOG_CURRENT));
            }
        }
        catch (Exception e)
        {
            plugin.getLogger().warning(this.language.getMessage(AstrelLanguage.LOG_UPDATE_FAIL));
        }

        return false;
    }

    public String getSourceVersionString()
    {
        return sourceVersionString;
    }

    public int getSourceVersion()
    {
        return sourceVersion;
    }

    public boolean isUpdateAvailable()
    {
        return updateAvailable;
    }
}
