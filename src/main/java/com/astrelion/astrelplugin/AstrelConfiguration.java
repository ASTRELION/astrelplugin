package com.astrelion.astrelplugin;

import org.bukkit.plugin.java.JavaPlugin;

public class AstrelConfiguration extends AstrelResource
{
    public static boolean ENABLE;
    public static boolean PROMPT_UPDATES;

    public AstrelConfiguration(JavaPlugin plugin)
    {
        super(plugin, "config.yml");

        if (!resource.contains("enable") ||
            !resource.contains("promptUpdates"))
        {
            throw new IllegalStateException("config.yml must contain 'enable' and 'promptUpdates' nodes.");
        }

        reload();
    }

    @Override
    public void reload()
    {
        super.reload();

        ENABLE = resource.getBoolean("enable");
        PROMPT_UPDATES = resource.getBoolean("promptUpdates");
    }
}
