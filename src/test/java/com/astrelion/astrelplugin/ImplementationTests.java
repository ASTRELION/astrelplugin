package com.astrelion.astrelplugin;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import co.aikar.commands.PaperCommandManager;
import co.aikar.commands.RootCommand;
import org.bukkit.plugin.PluginDescriptionFile;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

public class ImplementationTests
{
    private static ServerMock server;
    private static Example plugin;

    @BeforeAll
    static void setup()
    {
        server = MockBukkit.mock();
        plugin = MockBukkit.load(Example.class);
    }

    @AfterAll
    static void teardown()
    {
        MockBukkit.unmock();
        server = null;
        plugin = null;
    }

    @Test
    public void testBaseName()
    {
        assertEquals("example", AstrelUtil.baseName(plugin));
    }

    @Test
    public void testCommands()
    {
        PaperCommandManager commandManager = plugin.getCommandManager();

        for (RootCommand rootCommand : commandManager.getRegisteredRootCommands())
        {
            for (var command : rootCommand.getSubCommands().values())
            {
                for (Object p : command.getRequiredPermissions())
                {
                    assertTrue(p.toString().startsWith("example"));
                }
            }
        }
    }

    @Test
    public void testResourceManager()
    {
        // test.yml
        File testFile = new File(plugin.getDataFolder(), "test.yml");
        assertFalse(testFile.exists());
        AstrelResource testManager = new AstrelResource(plugin, "test.yml");
        assertTrue(testFile.exists());

        // config.yml
        File configFile = new File(plugin.getDataFolder(), "config.yml");
        assertTrue(configFile.exists());
        AstrelResource configManager = new AstrelResource(plugin, "config.yml");
        assertTrue(configFile.exists());

        // lang.yml
        File langFile = new File(plugin.getDataFolder(), "lang.yml");
        assertTrue(langFile.exists());
        AstrelResource langManager = new AstrelResource(plugin, "lang.yml");
        assertTrue(langFile.exists());
    }

    @Test
    public void testPluginYAML()
    {
        PluginDescriptionFile pdf = plugin.getDescription();
        assertEquals("Example", pdf.getName());
        assertEquals("com.astrelion.astrelplugin.Example", pdf.getMain());
        assertEquals("ASTRELION", pdf.getAuthors().get(0));
    }

    @Test
    public void testConfigYAML()
    {
        AstrelConfiguration configuration = plugin.getConfiguration();
        assertNotNull(configuration);
    }

    @Test
    public void testLangYAML()
    {
        AstrelLanguage language = plugin.getLanguage();
        assertNotNull(language);
    }

    @Test
    public void testUpdates()
    {
        assertNotNull(plugin.sourceURL);

        AstrelUpdate updateManager = plugin.getUpdateManager();
        updateManager.checkForUpdate();

        assertTrue(updateManager.isUpdateAvailable());
        assertTrue(updateManager.getSourceVersion() > 0);

        PlayerMock player1 = new PlayerMock(server, "player1");
        player1.setOp(true);
        server.addPlayer(player1);

        String msg = player1.nextMessage();
        assertNotNull(msg);
        assertEquals(plugin.getLanguage().getMessage(AstrelLanguage.UPDATE_PROMPT, plugin.sourceURL), msg);

        PlayerMock player2 = new PlayerMock(server, "player2");
        server.addPlayer(player2);

        assertNull(player2.nextMessage());
    }

    @Test
    public void testUtil()
    {
        assertEquals(0, AstrelUtil.clamp(0, -100, 100));
        assertEquals(-100, AstrelUtil.clamp(-200, -100, 100));
        assertEquals(100, AstrelUtil.clamp(200, -100, 100));

        assertEquals(60, AstrelUtil.secondsToTicks(3));
        assertEquals(3, AstrelUtil.ticksToSeconds(60));
    }

    @Test
    @Order(Integer.MAX_VALUE)
    public void testDisable()
    {
        server.getPluginManager().disablePlugin(plugin);
    }
}
