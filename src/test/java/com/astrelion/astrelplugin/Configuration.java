package com.astrelion.astrelplugin;

public class Configuration extends AstrelConfiguration
{
    private Example plugin;

    public Configuration(Example plugin)
    {
        super(plugin);
        this.plugin = plugin;
    }
}
