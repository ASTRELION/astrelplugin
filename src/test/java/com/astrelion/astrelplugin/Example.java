package com.astrelion.astrelplugin;

import co.aikar.commands.PaperCommandManager;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

import java.io.File;

public class Example extends JavaPlugin
{
    public final String sourceURL = "https://gitlab.com/ASTRELION/astrelplugin/-/releases/permalink/latest";

    private Configuration configuration;
    private Language language;
    private Permissions permissions;
    private AstrelUpdate updateManager;
    private PaperCommandManager commandManager;

    public Example()
    {
        super();
    }

    public Example(JavaPluginLoader loader, PluginDescriptionFile pdf, File dataFolder, File file)
    {
        super(loader, pdf, dataFolder, file);
    }

    @Override
    public void onEnable()
    {
        if (sourceURL.trim().equalsIgnoreCase(""))
        {
            getLogger().warning("Invalid Source URL!");
        }

        configuration = new Configuration(this);
        language = new Language(this);
        permissions = new Permissions(this);
        updateManager = new AstrelUpdate(this, language, sourceURL);
        commandManager = new PaperCommandManager(this);

        if (Configuration.ENABLE)
        {
            // enables the help command
            commandManager.enableUnstableAPI("help");
            // replaces %base with your plugin's basename
            // this determines the permission nodes requires for each command
            commandManager.getCommandReplacements().addReplacement(
                "base",
                AstrelUtil.baseName(this)
            );
            // adds an alias base for command usage
            // e.g. instead of `basealias help` you could use `foo help`
            commandManager.getCommandReplacements().addReplacement(
                "basealias",
                "foo"
            );
            // registers the default commands of AstrelCommand: help, reload, and version
            commandManager.registerCommand(new AstrelCommand(this, configuration, language, updateManager, sourceURL));
        }

        if (configuration == null ||
            permissions == null ||
            language == null)
        {
            throw new IllegalStateException("Configuration, language, or permissions not initialized!");
        }

        if (AstrelConfiguration.ENABLE)
        {
            getLogger().info(language.getMessage(AstrelLanguage.LOG_ENABLE));
        }
        else
        {
            getLogger().warning(language.getMessage(AstrelLanguage.LOG_DISABLE_CONFIG));
        }
    }

    @Override
    public void onDisable()
    {
        getServer().getScheduler().cancelTasks(this);
    }

    public Configuration getConfiguration()
    {
        return configuration;
    }

    public Permissions getPermissions()
    {
        return permissions;
    }

    public Language getLanguage()
    {
        return language;
    }

    public AstrelUpdate getUpdateManager()
    {
        return updateManager;
    }

    public PaperCommandManager getCommandManager()
    {
        return commandManager;
    }
}
